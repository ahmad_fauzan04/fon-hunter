package com.fon.ui;



import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;

public class Arrow {
	float rotation;
	int posX;
	int posY;
	Paint paint = new Paint();
	int radius = 25;
	
	public Arrow(int x, int y) {
		// TODO Auto-generated constructor stub
		posX = x;
		posY = y;
		paint.setColor(Color.WHITE);
		paint.setStyle(Style.FILL);
	}
	
	public void OnDraw(Canvas canvas) {
		
		canvas.rotate(rotation,posX, posY);
		Path path = new Path();
		path.setFillType(Path.FillType.EVEN_ODD);
		path.moveTo(posX , posY - radius);
		path.lineTo(posX - radius/2 , posY + radius);
		path.lineTo(posX + radius/2 , posY + radius);
		path.lineTo(posX , posY - radius);
		path.close();

		canvas.drawPath(path, paint);
	}
	
	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public int getRadius() {
		return radius;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
}
