package com.fon.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class SurfaceThread extends Thread {
	
	private SurfaceHolder surfaceHolder;
	private CallBack callBack;
	private long now;
	
	private boolean run = true;
	
	
	public SurfaceThread(SurfaceHolder surfaceHolder) {
		this.surfaceHolder = surfaceHolder;
		
	}
	
	@Override
	public void run() {
		while(run) {
			Canvas canvas = surfaceHolder.lockCanvas();
			if(canvas != null) {
				canvas.drawColor(Color.BLACK);
				if(callBack != null) {
					callBack.doUpdate((System.currentTimeMillis()-now)/1000.0f);
					now = System.currentTimeMillis();
					callBack.doDraw(canvas);
				}
			surfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}
	
	public void SetCallback(CallBack callback) {
		this.callBack = callback;
	}
	
	public interface CallBack {
		
		public void doUpdate(float deltaTime);
	
		public void doDraw(Canvas canvas);
		
		public void onMotionListener(MotionEvent event);
	}
	
	public void stopThread() {
		run = false;
	}
	

}
