package com.fon.ui;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;

import com.fon.gps.WifiReceiver;
import com.fon.gps.position.PositionListener;
import com.fon.net.*;
import com.fon.net.ConClient.Message;
import com.fon.radar.Treasure;
import com.fon.util.EarthPositionHelper;

public class Radar implements SurfaceThread.CallBack, ConClient.Listener, PositionListener {

	public enum Mode {
		RADAR, TRACKING
	};

	float direction;
	int width;
	int height;
	float scaleFactor = 1.0f;
	public Mode mode = Mode.RADAR;

	Paint paint = new Paint();
	Paint itemPaint = new Paint();

	float radiusEffect = 0;
	int maxRadiusEffect = 100;
	Hashtable<String, RadarObject> radarObject = new Hashtable<String, Radar.RadarObject>();
	RadarObject trackTreasure;
	ModeListener modeListener;
	Arrow arrow = null;
	
	float latitude = 0;
	float longitude = 0;
	
	Location location;
	
	SoundPool soundPool;
	int SoundID;
	int StreamID = -1;
	MediaPlayer mp;
	Context context;
	private float trackDistance = 0.0f;
	
	public WifiReceiver wifiR;
	
	public Radar(Context context) {
		this.context = context;
		paint.setColor(Color.GREEN);
		paint.setStyle(Paint.Style.STROKE);
		paint.setTextSize(24.0f);
		itemPaint.setColor(Color.BLUE);
		wifiR = new WifiReceiver((Activity)context);
		//InitDummy();
	}
	
	public void setSoundPool(SoundPool soundPool, int resIdSound) {
		this.soundPool = soundPool;
		SoundID = resIdSound;
	}
	
	public void InitDummy() {
		Treasure ts = new Treasure();
		ts.setID("aaa");
		ts.setData("bssid");
		ts.setDistance(75);
		ts.SetRotation(108);
		ts.setRadar(this);
		float[] pos = EarthPositionHelper.GetPosition(
				-6.056f, 107.01f, 50.0, 30.0);
		Log.d("Pos Radar", pos[0] + "," + pos[1]);
		ts.setLatitude(pos[0]);
		ts.setLongitude(pos[1]);
		radarObject.put("aaa", ts);
	}

	@Override
	public void doUpdate(float deltaTime) {
		
		radiusEffect += (deltaTime * maxRadiusEffect * scaleFactor);
		radiusEffect %= (maxRadiusEffect * scaleFactor);
		if(mode == Mode.TRACKING && arrow != null && location != null) {
			Location dest = new Location(location);
			dest.setLatitude(trackTreasure.getLatitude());
			dest.setLongitude(trackTreasure.getLongitude());
			float degree = location.bearingTo(dest);
			float distance = location.distanceTo(dest);
			arrow.setRotation(degree);
			float factor = (20/distance)<= 1.0f ? (20/distance) : 1.0f ;
			arrow.setRadius((int) ((100 - distance)*scaleFactor) );
			trackDistance = distance;
			if(soundPool != null && StreamID == -1) {
				StreamID = this.soundPool.play(SoundID, 1.0f, 1.0f, 1, -1, 0.5f + factor);
			} else {
				this.soundPool.setRate(StreamID, 0.5f + factor);
			}
		} else {
			
		}
		
	}

	@Override
	public void doDraw(Canvas canvas) {
		// Draw
		width = canvas.getWidth();
		height = canvas.getHeight();
		scaleFactor = canvas.getWidth() / (2 * 100);
		
		
		canvas.drawColor(Color.BLACK);
		canvas.drawText("latitude : " + latitude, 10, 100, paint);
		canvas.drawText("longitude : " + longitude, 10, 120, paint);
		if(mode == Mode.TRACKING) {
			canvas.drawText("trackDistance : " + trackDistance, 10, 140, paint);
			canvas.drawText("Wifi Strength : " + wifiR.getWifi(trackTreasure.getData().toString()), 10, 160, paint);
		}
		//canvas.rotate(-direction, width / 2, height / 2);
		for(int i=0; i<110; i+=10) { 
			canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2,
					i * scaleFactor, paint);
		}		
		canvas.drawLine(canvas.getWidth() / 2, canvas.getHeight() / 2 - 100
				* scaleFactor, canvas.getWidth() / 2, canvas.getHeight() / 2
				+ 100 * scaleFactor, paint);
		canvas.drawLine(canvas.getWidth() / 2 - 100 * scaleFactor,
				canvas.getHeight() / 2, canvas.getWidth() / 2 + 100
						* scaleFactor, canvas.getHeight() / 2, paint);

		if (mode == Mode.RADAR) {
			for (int i = 0; i < radarObject.size(); i++) {
				RadarObject obj = radarObject.get(radarObject.keySet()
						.toArray()[i]);
				canvas.drawCircle(canvas.getWidth() / 2 + obj.getX()
						* scaleFactor, canvas.getHeight() / 2 + obj.getY()
						* scaleFactor, 5 * scaleFactor, itemPaint);
			}
		} else {
			canvas.rotate(-direction, width / 2, height / 2);
			arrow.OnDraw(canvas);
		}

		canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2,
				radiusEffect, paint);

	}

	public interface RadarObject {

		public int getX();

		public int getY();

		public double getDistance();

		public void setDistance(double distance);

		public int getRotation();

		public void SetRotation(int rotInDegree);

		public void setData(Object data);

		public Object getData();

		public void setRadar(Radar radar);

		public Radar getRadar();

		public boolean isTouch(int x, int y);

		public void setID(String ID);

		public String getID();
		
		public void setLatitude(float latitude);
		
		public void setLongitude(float longitude);
		
		public float getLatitude();
		
		public float getLongitude();
	}

	public RadarObject getTrackTreasure() {
		return trackTreasure;
	}
	
	@Override
	public void GetMessage(Message msg) {
		Log.d("Radar", "Hello MSG");
		// I like to move it move it
		if (msg.getType() == Message.CHESTCOUNT) {

		} else if (msg.getType() == Message.CHESTSLOCATION) {
			if (msg.getResponse() != null)
				try {
					float[] data = (float[]) msg.getData();
					JSONArray treasures = msg.getResponse()
							.getJSONArray("data");
					if (treasures == null)
						return;
					if(treasures.length() > 0) 
						radarObject.clear();
					
					for (int i = 0; i < treasures.length(); i++) {
						String id = treasures.getJSONObject(i).getString("id");
						String bssid = treasures.getJSONObject(i).getString(
								"bssid");
						double distance = treasures.getJSONObject(i).getDouble(
								"distance");
						int degree = treasures.getJSONObject(i)
								.getInt("degree");
						if (!radarObject.contains(id)) {
							Treasure ts = new Treasure();
							ts.setID(id);
							ts.setData(bssid);
							ts.setDistance(distance);
							ts.SetRotation(degree);
							ts.setRadar(this);
							float[] pos = EarthPositionHelper.GetPosition(
									data[0], data[1], distance, degree);
							Log.d("Before Pos  Radar", data[0] + "," + data[1]);
							Log.d("Pos Radar", pos[0] + "," + pos[1]);
							ts.setLatitude(pos[0]);
							ts.setLongitude(pos[1]);
							radarObject.put(id, ts);
						} else {
							Treasure ts = (Treasure) radarObject.get(id);
							ts.setID(id);
							ts.setData(bssid);
							ts.setDistance(distance);
							ts.SetRotation(degree);
							ts.setRadar(this);
							float[] pos = EarthPositionHelper.GetPosition(
									data[0], data[1], distance, degree);
							Log.d("Pos Radar", pos[0] + "," + pos[1]);
							ts.setLatitude(pos[0]);
							ts.setLongitude(pos[1]);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}

	}

	@Override
	public void onMotionListener(MotionEvent event) {
		if (mode != Mode.RADAR)
			return;
		for (int i = 0; i < radarObject.size(); i++) {
			RadarObject obj = radarObject
					.get(radarObject.keySet().toArray()[i]);
			
			if (event.getX() > getWidth() / 2 + obj.getX() * scaleFactor - 5
					* scaleFactor
					&& event.getX() < getWidth() / 2 + obj.getX() * scaleFactor
							+ 5 * scaleFactor
					&& event.getY() > getHeight() / 2 + obj.getY()
							* scaleFactor - 5 * scaleFactor
					&& event.getY() < getHeight() / 2 + obj.getY()
							* scaleFactor + 5 * scaleFactor) {
				setMode(Mode.TRACKING);
				trackTreasure = obj;

				// Get Current Position

				// Add Dest Position

				// Track With GPS
				
				if(arrow == null) {
					arrow = new Arrow(width/2, height/2);
				}
				return;
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public interface ModeListener {
		void OnRadarModeChange(Mode mode);
	}

	public void setModeListener(ModeListener modeListener) {
		this.modeListener = modeListener;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
		if (this.modeListener != null)
			modeListener.OnRadarModeChange(mode);
		if(mode == Mode.RADAR) {
			if(soundPool != null) {
				soundPool.stop(StreamID);
				StreamID = -1;
			}
		}
	}

	public void setDirection(float direction) {
		this.direction = direction;
	}
	
	public void setLatLong(float latitude, float longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public void OnLocationChange(Location location) {
		// TODO Auto-generated method stub
		this.location = location;
	}
}
