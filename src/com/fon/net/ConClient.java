package com.fon.net;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

@SuppressWarnings("deprecation")
public class ConClient {

	private final String groupID = "1f3c7c747a9caba02cbd853bd9328e6e";
	private final String uri = "http://milestone.if.itb.ac.id/pbd/index.php";
	private List<Message> QueueMessage = new ArrayList<ConClient.Message>();
	private List<MessageToSent> QueueSentMessage = new ArrayList<MessageToSent>();
	private Listener listener;
	public boolean isRunning = false;
	
	public ConClient() {

	}

	public void setListener(Listener l) {
		this.listener = l;
	}
	
	public JSONObject ResetChest() {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(uri);

		List<NameValuePair> dataValuePairs = new ArrayList<NameValuePair>(2);
		dataValuePairs.add(new BasicNameValuePair("group_id", groupID));
		dataValuePairs.add(new BasicNameValuePair("action", "reset"));

		JSONObject res = null;

		try {
			httppost.setEntity(new UrlEncodedFormEntity(dataValuePairs));
			Log.d("FON", "Work");

			try {
				HttpResponse response = httpclient.execute(httppost);
				String responseString = EntityUtils.toString(response
						.getEntity());
				Log.d("FON", "Response : " + responseString);
				res = new JSONObject(responseString);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return res;
	}

	public JSONObject GetChestCount() {
		HttpResponse response = Get(uri + "?group_id=" + groupID
				+ "&action=number");
		JSONObject res = null;

		if (response != null)
			try {
				String responseString = EntityUtils.toString(response
						.getEntity());
				Log.d("FON", "Response : " + responseString);
				res = new JSONObject(responseString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return res;
	}

	public JSONObject GetChestLocation(float _lat, float _long) {
		HttpResponse response = Get(uri + "?group_id=" + groupID
				+ "&action=retrieve&latitude=" + _lat + "&longitude=" + _long);
		JSONObject res = null;
		if (response != null)
			try {
				String responseString = EntityUtils.toString(response
						.getEntity());
				Log.d("FON", "Response : " + responseString);
				res = new JSONObject(responseString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return res;
	}

	public void GetChestCountAsync() {
		HttpGet httpget = new HttpGet(uri + "?group_id=" + groupID
				+ "&action=number");
		MessageToSent msgToSent = new MessageToSent();
		msgToSent.type = Message.CHESTCOUNT;
		msgToSent.httpGet = httpget;
		QueueSentMessage.add(msgToSent);
	}

	public void GetChestLocationAsync(float _lat, float _long) {
		HttpGet httpget = new HttpGet(uri + "?group_id=" + groupID
				+ "&action=retrieve&latitude=" + _lat + "&longitude=" + _long);
		MessageToSent msgToSent = new MessageToSent();
		msgToSent.type = Message.CHESTSLOCATION;
		msgToSent.httpGet = httpget;
		float[] data = new float[2];
		data[0] = _lat;
		data[1] = _long;
		msgToSent.data = data;
		QueueSentMessage.add(msgToSent);
	}

	public JSONObject AcquireChest(String chest_id, File image, String bssid,
			float wifi) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(uri);
		JSONObject res = null;

		MultipartEntity entity = new MultipartEntity();
		try {
			entity.addPart("group_id", new StringBody(groupID));
			entity.addPart("action", new StringBody("acquire"));
			entity.addPart("chest_id", new StringBody(chest_id));
			entity.addPart("bssid", new StringBody(bssid));
			entity.addPart("wifi", new StringBody(Float.toString(wifi)));
			entity.addPart("file", new FileBody(image));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		httppost.setEntity(entity);
		try {
			String dataScript = EntityUtils.toString(entity);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.d("FON", "Work");

		try {
			HttpResponse response = httpclient.execute(httppost);
			String responseString = EntityUtils.toString(response.getEntity());
			Log.d("FON", "Response : " + responseString);
			res = new JSONObject(responseString);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return res;
	}

	public HttpResponse Get(String uri) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(uri);

		try {
			HttpResponse response = httpclient.execute(httpget);
			return response;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public class Message {
		public final static int RESETCHEST = 0;
		public final static int CHESTCOUNT = 1;
		public final static int CHESTSLOCATION = 2;
		public final static int ACQUIRE_CHEST = 3;

		private int type;
		private JSONObject response;
		private Object data;
		
		public int getType() {
			return type;
		}

		public JSONObject getResponse() {
			return response;
		}
		
		public Object getData() {
			return data;
		}
	}
	
	private class MessageToSent {
		private int type;
		//private HttpPost httpPost;
		private HttpGet httpGet;
		private Object data;
	}

	void SentHandler() {
		while (isRunning) {
			if (!QueueSentMessage.isEmpty()) {
				MessageToSent msgToSent = QueueSentMessage.remove(0);
				Message msg = new Message();
				msg.type = msgToSent.type;
				msg.data = msgToSent.data;
				msg.response = null;
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = null;
				String responseString = "";
				switch (msgToSent.type) {
				case Message.CHESTCOUNT:
				case Message.CHESTSLOCATION:
					try {
						response = httpclient.execute(msgToSent.httpGet);
						responseString = EntityUtils.toString(response.getEntity());
						msg.response = new JSONObject(responseString);
						Log.d("Response", responseString);
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ParseException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					break;

				default:
					break;
				}
				
				QueueMessage.add(msg);
			}
		}
	}

	void GetResponseHandler() {
		while (isRunning) {
			if (!QueueMessage.isEmpty()) {
				Message msg = QueueMessage.remove(0);
				if(listener != null)
					listener.GetMessage(msg);
			}
		}
	}

	public interface Listener {
		void GetMessage(Message msg);
	}
	
	public void Start() {
		isRunning = true;
		(new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				SentHandler();
			}
		})).start();
		(new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				GetResponseHandler();
			}
		})).start();
	}
	
	public void Stop() {
		isRunning = false;
	}
}
