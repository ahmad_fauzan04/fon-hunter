package com.fon.util;

public class EarthPositionHelper {
	
	static float earthRadius = 6371.0f;
	static float RAD_TO_DEGREE = (float) (180.0/Math.PI);
	static float DEG_TO_RAD = (float) (Math.PI/180.0);
	
	public static float[] GetPosition(float _lat, float _long, double dist, double degree) {
		float[] pos = new float[2];
		//Lat
		double lat2 = Math.asin(Math.sin(_lat*DEG_TO_RAD)*Math.cos((dist/1000.0)/earthRadius) + Math.cos(_lat*DEG_TO_RAD)*Math.sin((dist/1000.0)/(earthRadius))*Math.cos(degree*DEG_TO_RAD)) * RAD_TO_DEGREE;
		pos[0] = (float) lat2;
		//Long
		pos[1] = (float) (_long*DEG_TO_RAD + Math.atan2(Math.sin(degree*DEG_TO_RAD)*Math.sin((dist/1000.0)/(earthRadius))*Math.cos(_lat*DEG_TO_RAD), Math.cos((dist/1000.0)/(earthRadius))-Math.sin(_lat*DEG_TO_RAD)*Math.sin(pos[0]*DEG_TO_RAD))) * RAD_TO_DEGREE;
		return pos;
	}
}
