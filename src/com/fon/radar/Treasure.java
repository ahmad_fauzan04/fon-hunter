package com.fon.radar;

import com.fon.ui.Radar;
import com.fon.ui.Radar.RadarObject;

public class Treasure implements RadarObject{
	String id = "";
	String bssid = "";
	double distance = 0;
	int degree = 0;
	Radar radar;
	float latitude;
	float longitude;
	// GW ganteng parah.com
	
	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return (int)(distance * Math.cos(Math.PI*(degree-90)/180));
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return (int)(distance * Math.sin(Math.PI*(degree-90)/180));
	}

	@Override
	public double getDistance() {
		// TODO Auto-generated method stub
		return distance;
	}

	@Override
	public int getRotation() {
		// TODO Auto-generated method stub
		return degree;
	}

	@Override
	public Object getData() {
		// TODO Auto-generated method stub
		return bssid;
	}

	@Override
	public void setRadar(Radar radar) {
		// TODO Auto-generated method stub
		this.radar = radar;
	}

	@Override
	public Radar getRadar() {
		// TODO Auto-generated method stub
		return radar;
	}

	@Override
	public boolean isTouch(int x, int y) {
		// TODO Auto-generated method stub
		return x > getX()-5 && y > getY()-5 && x < getX()+5 && y < getY()+5;
	}

	@Override
	public void setDistance(double distance) {
		// TODO Auto-generated method stub
		this.distance = distance;
	}

	@Override
	public void SetRotation(int rotInDegree) {
		// TODO Auto-generated method stub
		degree = rotInDegree;
	}

	@Override
	public void setData(Object data) {
		// TODO Auto-generated method stub
		bssid = data.toString();
	}

	@Override
	public void setID(String ID) {
		// TODO Auto-generated method stub
		id = ID;
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return id;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	
	public float getLatitude() {
		return latitude;
	}
	
	public float getLongitude() {
		return longitude;
	}
}
