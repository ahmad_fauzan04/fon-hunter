package com.fon.fon_hnter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.fon.net.ConClient;
import com.fon.ui.Radar;
import com.fon.ui.Radar.Mode;
import com.fon.ui.Radar.ModeListener;
import com.fon.ui.SurfacePanel;

import com.fon.gps.WifiReceiver;
import com.fon.gps.position;

import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity implements ModeListener,
		SensorEventListener {

	ConClient client;
	Radar radar;
	position Pos;

	private boolean sersorrunning;
	private static SensorManager mySensorManager;
	private static final int CAMERA_REQUEST = 1888;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitNetwork().build();
			StrictMode.setThreadPolicy(policy);
		}

		Button photoButton = (Button) this.findViewById(R.id.checkinButton);
		photoButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent cameraIntent = new Intent(
						android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, CAMERA_REQUEST);
			}
		});

		// Test
		client = new ConClient();
		JSONObject response = client.ResetChest();
		if (response != null)
			try {
				if (response.getString("status").equals("failed")) {
					Toast.makeText(this, response.getString("description"),
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
		int iSound = soundPool.load(getApplicationContext(), R.raw.blip, 1);

		Pos = new position(this);
		SurfacePanel panel = (SurfacePanel) findViewById(R.id.surfaceView1);
		radar = new Radar(this);
		panel.SetCallback(radar);
		client.setListener(radar);
		radar.setModeListener(this);
		Pos.setPositionListener(radar);
		radar.setSoundPool(soundPool, iSound);

		float lat = Pos.getLang();
		float lon = Pos.getLong();

		Log.d("apapun", lat + "," + lon);

		client.Start();
		client.GetChestCountAsync();
		client.GetChestLocationAsync(Pos.getLang(), Pos.getLong());
		// client.GetChestLocation(-6.890608f, 107.610008f);

		mySensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		@SuppressWarnings("deprecation")
		List<Sensor> mySensors = mySensorManager
				.getSensorList(Sensor.TYPE_ORIENTATION);

		if (mySensors.size() > 0) {
			mySensorManager.registerListener(this, mySensors.get(0),
					SensorManager.SENSOR_DELAY_NORMAL);
			sersorrunning = true;
			Toast.makeText(this, "Start ORIENTATION Sensor", Toast.LENGTH_LONG)
					.show();
		} else {
			Toast.makeText(this, "No ORIENTATION Sensor", Toast.LENGTH_LONG)
					.show();
			sersorrunning = false;
			finish();
		}
		Button refreshBtn = (Button) findViewById(R.id.refreshButton);
		refreshBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Pos.updateLocation();
				client.GetChestLocationAsync(Pos.getLang(), Pos.getLong());
				radar.setLatLong(Pos.getLang(), Pos.getLong());
			}
		});

		Button resetBtn = (Button) findViewById(R.id.resetButton);
		resetBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				client.ResetChest();
				Pos.updateLocation();
				client.GetChestLocationAsync(Pos.getLang(), Pos.getLong());
				radar.setLatLong(Pos.getLang(), Pos.getLong());
			}
		});
	}
<<<<<<< HEAD
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photo = Bitmap.createScaledBitmap(photo, 640, 480, true);
            String filepath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/fonHunter";
            File dir = new File(filepath);
            if (!dir.exists()) {
            	dir.mkdirs();
            	File file = new File(dir, "fonhunter.JPEG" );
            	FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(file);
					photo.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
	            	fOut.flush();
	            	fOut.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            }
            
            geoTag(dir.getAbsolutePath()+"/fonhunter.JPEG",Pos.getLang(), Pos.getLong());
            
            
        }  
    } 
	
	public void geoTag(String filename, double latitude, double longitude){
	    ExifInterface exif;

	    try {
	        exif = new ExifInterface(filename);
	        int num1Lat = (int)Math.floor(latitude);
	        int num2Lat = (int)Math.floor((latitude - num1Lat) * 60);
	        double num3Lat = (latitude - ((double)num1Lat+((double)num2Lat/60))) * 3600000;

	        int num1Lon = (int)Math.floor(longitude);
	        int num2Lon = (int)Math.floor((longitude - num1Lon) * 60);
	        double num3Lon = (longitude - ((double)num1Lon+((double)num2Lon/60))) * 3600000;

	        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, num1Lat+"/1,"+num2Lat+"/1,"+num3Lat+"/1000");
	        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, num1Lon+"/1,"+num2Lon+"/1,"+num3Lon+"/1000");


	        if (latitude > 0) {
	            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N"); 
	        } else {
	            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
	        }

	        if (longitude > 0) {
	            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");    
	        } else {
	        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
	        }

	        exif.saveAttributes();
	    } catch (IOException e) {
	        Log.e("PictureActivity", e.getLocalizedMessage());
	    } 

	    }
	
=======

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
			Bitmap photo = (Bitmap) data.getExtras().get("data");
			photo = Bitmap.createScaledBitmap(photo, 640, 480, true);
			String filepath = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/fonHunter";
			File dir = new File(filepath);
			if (!dir.exists())
				dir.mkdirs();
			File file = new File(dir, "fonhunter.JPEG");
			FileOutputStream fOut;
			try {
				fOut = new FileOutputStream(file);
				photo.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
				fOut.flush();
				fOut.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				ExifInterface exif = new ExifInterface(dir.getAbsolutePath() + "/fonhunter.JPEG");
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE,
						Float.toString(Pos.getLang()));
				exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,
						Float.toString(Pos.getLong()));
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
				

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			OnAccuireChest(file);
		}
	}
>>>>>>> aedcdd183820dc9256599ea7c2ee9e843af87a2a

	SoundPool soundPool;

	@Override
	protected void onStart() {
		super.onStart();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		if (radar.mode == Mode.TRACKING) {
			radar.setMode(Mode.RADAR);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void OnRadarModeChange(Mode mode) {
		LinearLayout radarLayout = (LinearLayout) findViewById(R.id.radarMode);
		LinearLayout trackingLayout = (LinearLayout) findViewById(R.id.trackingMode);

		if (mode == Mode.RADAR) {
			radarLayout.setVisibility(View.VISIBLE);
			trackingLayout.setVisibility(View.INVISIBLE);
		} else {
			radarLayout.setVisibility(View.INVISIBLE);
			trackingLayout.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		radar.setDirection((float) event.values[0]);
		// Log.d("Sensor", "radar : " + event.values[0]);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (sersorrunning) {
			mySensorManager.unregisterListener(this);
		}
		client.Stop();

	}

	private void OnAccuireChest(File file) {
		// prepare for a progress bar dialog
		final ProgressDialog progressBar = new ProgressDialog(this);
		progressBar.setCancelable(true);
		progressBar.setMessage("File downloading ...");
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgress(0);
		progressBar.setMax(100);
		progressBar.show();

		JSONObject result = client.AcquireChest(radar.getTrackTreasure()
				.getID(), file, radar.getTrackTreasure().getData().toString(),
				radar.wifiR.getWifi(radar.getTrackTreasure().getData()
						.toString()));

		progressBar.dismiss();

		try {
			if (result.getString("status").equalsIgnoreCase("success")) {
				Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this,
						"failed : " + result.getString("description"),
						Toast.LENGTH_LONG).show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
