package com.fon.gps;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

public class WifiReceiver {
	WifiManager mainWifi;
	
	public WifiReceiver (Activity context) {
		mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	}
	
	public float getWifi (String wifiID) {
		// TODO Auto-generated method stub
		float signStrength = 0;
		if(mainWifi.startScan()) {}
			List<ScanResult> results = mainWifi.getScanResults();
			
			if (results != null) {
				for (int i = 0; i < results.size(); i++) {
					ScanResult findSignal = results.get(i);
					if (findSignal.BSSID.equalsIgnoreCase(wifiID)) {
						signStrength = findSignal.level;
					}
				}
			}
		return signStrength;
	}
}
