package com.fon.gps;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class position implements LocationListener {
	
	private float langtd;
	private float longtd;
	private LocationManager locationManager;
	private String provider;
	PositionListener positionListener;

	public position (Activity context) {
		//get location manager
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
	    provider = LocationManager.GPS_PROVIDER;
	    Location location = locationManager.getLastKnownLocation(provider);
	    
	    if (location != null) {
	        onLocationChanged(location);
	    }
	    
	    locationManager.requestLocationUpdates(provider, 1000, 0, this); 
	}
	
	@Override
	public void onLocationChanged(Location location) {
	    langtd = (float) (location.getLatitude());
	    longtd = (float) (location.getLongitude());
	    if(positionListener != null) positionListener.OnLocationChange(location);
	}
	
	public void setPositionListener(PositionListener positionListener) {
		this.positionListener = positionListener;
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	    // TODO Auto-generated method stub

	  }
	
	@Override
	public void onProviderEnabled(String provider) {
	    /*Toast.makeText(this, "Enabled new provider " + provider,
	        Toast.LENGTH_SHORT).show();*/
	}

	@Override
	public void onProviderDisabled(String provider) {
	    /*Toast.makeText(this, "Disabled provider " + provider,
	        Toast.LENGTH_SHORT).show();*/

	}
	
	public void updateLocation() {
	    Location location = locationManager.getLastKnownLocation(provider);
		if (location != null) {
	        onLocationChanged(location);
	      }
	}
	
	public float getLang () {
		return langtd;
	}
	
	public float getLong () {
		return longtd;
	}
	
	public interface PositionListener {
		void OnLocationChange(Location location);
	}
}
